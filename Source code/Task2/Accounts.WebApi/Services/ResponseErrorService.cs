﻿using Accounts.WebApi.Interfaces;
using Base.WebApi.Services;
using Common.Library.Models;

namespace Accounts.WebApi.Services
{
    internal class ResponseErrorService : BaseResponseError, IResponseErrorService
    {
        public ResponseError UserNotFound()
        {
            return new ResponseError("user_not_found", "User not found");
        }
    }
}
