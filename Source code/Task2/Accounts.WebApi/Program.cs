﻿using Base.WebApi;
using Microsoft.AspNetCore.Hosting;

namespace Accounts.WebApi
{
    public class Program : BaseProgram
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .UseStartup<Startup>()
                .Build()
                .Run();
        }        
    }
}
