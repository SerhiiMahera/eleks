﻿using System.Collections.Generic;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace Accounts.WebApi.Configs
{
    public static class Config
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("Products", "My product API"),
                new ApiResource("Accounts", "My account API"),
                new ApiResource("Baskets", "My basket API"),
                new ApiResource("Files", "My file API")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "Web",
                    ClientName = "Web client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("7ccb2779-deb1-45a3-9205-cefcfcf63f1d".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "Products", "Accounts", "Baskets", "Files" },
                    AllowOfflineAccess = true,
                },
                new Client
                {
                    ClientId = "Swagger",
                    ClientName = "Swagger client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("8eaa4e3f-660d-4e0d-8ceb-8befd7d4f5a5".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "Products", "Accounts", "Baskets", "Files" },
                    AllowOfflineAccess = true, 
                }
            };
        }
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password"
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password"
                }
            };
        }
    }
}
