﻿using Common.Library.Interfaces;
using Common.Library.Models;

namespace Accounts.WebApi.Interfaces
{
    internal interface IResponseErrorService : IBaseResponseError
    {
        ResponseError UserNotFound();
    }
}