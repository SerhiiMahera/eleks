﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Accounts.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetAccountsAsync()
        {
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccountAsync([FromRoute]string id)
        {
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddAccountAsync()
        {
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAccountAsync()
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccountAsync([FromRoute]string id)
        {
            return Ok();
        }
    }
}