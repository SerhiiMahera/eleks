﻿using System;
using System.IO;

namespace Files.Common.Interfaces
{
    public interface IFileInfo
    {
        Guid Id { get; }
        Stream FileStream { get; }
        DateTime Date { get; }
        bool IsConfirmed { get; }
        string ContentType { get; }
    }
}