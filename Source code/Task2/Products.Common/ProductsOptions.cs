﻿using Common.Library.Models;
using Newtonsoft.Json;

namespace Products.Common
{
    public class ProductsOptions : SearchOptions
    {
        public ProductsOrderBy SortBy { get; set; }
        
        public string Ordering() => SortBy + " " + OrderBy();
    }
}
