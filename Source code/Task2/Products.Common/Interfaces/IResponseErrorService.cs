﻿using Common.Library.Interfaces;
using Common.Library.Models;

namespace Products.Common.Interfaces
{
    public interface IResponseErrorService : IBaseResponseError
    {
        ResponseError ProductNotFound();
    }
}
