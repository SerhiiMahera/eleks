﻿using System;
using Products.Common.Interfaces;

namespace Products.Common.Models
{
    public class CreateProductRequest : IProduct
    {
        public Guid ImageId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
