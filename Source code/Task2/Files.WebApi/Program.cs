﻿using Base.WebApi;
using Microsoft.AspNetCore.Hosting;

namespace Files.WebApi
{
    public class Program : BaseProgram
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }
    }
}
