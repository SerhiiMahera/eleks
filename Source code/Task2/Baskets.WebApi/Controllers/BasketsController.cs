﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Baskets.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketsController : ControllerBase
    {
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBasketAsync()
        {
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddProductAsync()
        {
            return Ok();
        }

        [HttpPut("{productid}")]
        public async Task<IActionResult> UpdateProductCountAsync([FromRoute]int productid, [FromBody]uint count)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductAsync([FromRoute]int id)
        {
            return Ok();
        }
    }
}
