﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Products.BLL.Interfaces;
using Products.Common;
using Products.Common.Interfaces;
using Products.Common.Models;

namespace Products.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsService _products;
        private readonly IResponseErrorService _errorService;

        public ProductsController(IProductsService products, IResponseErrorService errorService)
        {
            _products = products ?? throw new ArgumentNullException(nameof(products));
            _errorService = errorService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProductsAsync([FromQuery]ProductsOptions options)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(_errorService.ModelStateNotValid(ModelState));
            }

            var response = await _products.GetProductsByFilterAsync(options)
                .ConfigureAwait(false);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductsAsync(int id)
        {
            var response = await _products.GetProductByIdAsync(id)
                .ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> AddProductAsync(CreateProductRequest request)
        {
            await _products
                .AddAsync(request)
                .ConfigureAwait(false);

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProductAsync([FromRoute]int id, CreateProductRequest request)
        {
            await _products.EditAsync(id, request)
                .ConfigureAwait(false);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductAsync(int id)
        {
            await _products
                .RemoveAsync(id)
                .ConfigureAwait(false);
            return Ok();
        }
    }
}