﻿namespace Base.DAL.Interfaces
{
    public interface IModelId<T>
    {
        T Id { get; set; }
    }
}