﻿using System;
using Products.Common.Interfaces;

namespace Products.DAL.Entities
{
    public class Product : IProduct
    {
        public int Id { get; set; }
        public Guid ImageId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }

        public Product()
        {
            
        }

        public Product(IProduct product)
        {
            Id = product.Id;
            ImageId = product.ImageId;
            Name = product.Name;
            Price = product.Price;
            Amount = product.Amount;
            Description = product.Description;
        }
    }
}
