﻿using System;
using System.Collections.Generic;
using System.Linq;
using Products.DAL.Entities;
using Products.DAL.Interfaces;
using System.Threading.Tasks;
using Base.WebApi.Exceptions;
using Products.Common.Interfaces;
using Products.Common;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;

namespace Products.DAL.Repositories
{
    public class MockProductRepository : IProductRepository
    {
        private static readonly List<IProduct> _products = new List<IProduct>()
        {
            new Product
            {
                Id = 1,
                Amount = 10,
                Description = "It is test product #1",
                Name = "Car",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 2,
                Amount = 32,
                Description = "It is test product #1",
                Name = "Book",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 3,
                Amount = 2,
                Description = "It is test product #1",
                Name = "Table",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 4,
                Amount = 546,
                Description = "It is test product #1",
                Name = "Sofa",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 5,
                Amount = 25,
                Description = "It is test product #1",
                Name = "Chair",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 6,
                Amount = 10,
                Description = "It is test product #1",
                Name = "Phone",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 7,
                Amount = 34,
                Description = "It is test product #1",
                Name = "Skirt",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 8,
                Amount = 67,
                Description = "It is test product #1",
                Name = "Carpet",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            },
            new Product
            {
                Id = 9,
                Amount = 11,
                Description = "It is test product #1",
                Name = "Notepad",
                ImageId = Guid.NewGuid(),
                Price = 10.0m
            }
        };

        public MockProductRepository()
        {
        }

        public Task<IProduct> GetByIdAsync(int id)
        {
            var prod = _products.Find(x => x.Id.Equals(id));
            return Task.FromResult(prod);
        }

        public Task<List<IProduct>> GetManyAsync(Expression<Func<IProduct, IProduct>> selector = null, Expression<Func<IProduct, bool>> predicate = null, Func<IQueryable<IProduct>, IOrderedQueryable<IProduct>> orderBy = null, Func<IQueryable<IProduct>, IIncludableQueryable<IProduct, object>> include = null,
            bool disableTracking = false, int skip = 0, int take = 20)
        {
            throw new NotImplementedException();
        }

        public Task<IProduct> GetFirstOrDefaultAsync(Expression<Func<IProduct, IProduct>> selector, Expression<Func<IProduct, bool>> predicate = null, Func<IQueryable<IProduct>, IIncludableQueryable<IProduct, object>> include = null,
            bool disableTracking = false)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AnyAsync(Expression<Func<IProduct, bool>> predicate = null)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(IProduct entity)
        {
            _products.Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(IProduct entity)
        {
            var prodIndex = _products.FindIndex(x => x.Id.Equals(entity.Id));
            if (prodIndex != -1)
            {
                _products[prodIndex] = entity;
            }

            return Task.CompletedTask;
        }

        public Task RemoveAsync(int id)
        {
            var prod = GetByIdAsync(id).Result;
            if (prod != null)
                _products.Remove(prod);
            return Task.CompletedTask;
        }

        public Task<List<IProduct>> GetByFilterAsync(ProductsOptions options)
        {
            return Task.FromResult(_products
                .Where(x => x.Name.Contains(options.Search))
                .Skip(options.Skip)
                .Take(options.Take)
                .ToList());
        }
    }
}