﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Products.Common.Interfaces;
using Products.DAL.Context;
using Products.DAL.Interfaces;
using System.Linq.Dynamic.Core;
using Base.DAL.Repositories;
using Products.Common;

namespace Products.DAL.Repositories
{
    public class EFProductRepository : BaseRepository<IProduct, int, ProductsContext>, IProductRepository
    {
        public EFProductRepository(ProductsContext db) : base(db)
        {
        }

        public Task<List<IProduct>> GetByFilterAsync(ProductsOptions options)
        {
          return GetManyAsync(predicate: x => x.Name.Contains(options.Search),
                         orderBy: ob => ob.OrderBy(options.Ordering()),
                         skip: options.Skip,
                         take: options.Take);
        }
    }
}
