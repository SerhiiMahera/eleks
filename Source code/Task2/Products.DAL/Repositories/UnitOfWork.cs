﻿using System;
using System.Threading.Tasks;
using Products.DAL.Context;
using Products.DAL.Interfaces;

namespace Products.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;
        private IProductRepository _products;
        private readonly ProductsContext _context;

        public IProductRepository Products => _products ?? (_products = new EFProductRepository(_context));

        public UnitOfWork(ProductsContext context)
        {
            _context = context;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task<int> SaveAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}