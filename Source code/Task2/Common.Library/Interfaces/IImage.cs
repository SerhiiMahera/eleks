﻿using System;

namespace Common.Library.Interfaces
{
    public interface IImage
    {
        Guid ImageId { get; set; }
    }
}