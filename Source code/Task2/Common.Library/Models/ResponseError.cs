﻿namespace Common.Library.Models
{
    public class ResponseError
    {
        public string StatusCode { get; set; }
        public string Description { get; set; }

        public ResponseError(string statusCode, string description)
        {
            StatusCode = statusCode;
            Description = description;
        }
    }
}
