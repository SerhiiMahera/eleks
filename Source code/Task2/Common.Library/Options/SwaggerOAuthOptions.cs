﻿namespace Common.Library.Options
{
    public class SwaggerOAuthOptions
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
