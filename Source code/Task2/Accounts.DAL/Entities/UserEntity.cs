﻿using Accounts.DAL.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Accounts.DAL.Entities
{
    public class UserEntity : IdentityUser, IUserEntity
    {
    }
}
