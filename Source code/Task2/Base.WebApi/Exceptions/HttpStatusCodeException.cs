﻿using System;
using Common.Library.Models;

namespace Base.WebApi.Exceptions
{
    public class HttpStatusCodeException : Exception
    {
        public ResponseError Response { get; private set; }

        public HttpStatusCodeException(ResponseError response)
        {
            Response = response;
        }
    }
}
