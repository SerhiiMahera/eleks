﻿using System.Threading;
using System.Threading.Tasks;
using Base.WebApi.Models;

namespace Base.WebApi.Interfaces
{
    public interface IHealthCheck
    {
        Task<HealthcheckResult> Check(CancellationToken cancellationToken = default(CancellationToken));
    }
}