﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Base.WebApi
{
    public class BaseProgram
    {
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseStartup<BaseStartup>();
    }
}
