﻿using Common.Library.Enums;

namespace Base.WebApi.Models
{
    public class HealthcheckResult
    {
        public HealthStatus Status { get; }
        public string Description { get; }

        public HealthcheckResult(HealthStatus status, string description = "ok")
        {
            Status = status;
            Description = description;
        }
    }
}
