﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Base.WebApi.Interfaces;
using Base.WebApi.Models;

namespace Base.WebApi.Builders
{
    public class HealthcheckBuilder
    {
        private readonly ICollection<IHealthCheck> _checkCollection;

        public HealthcheckBuilder()
        {
            _checkCollection = new List<IHealthCheck>();
        }

        public void AddCheck(IHealthCheck checkFunc)
        {
            if (checkFunc == null)
                throw new ArgumentNullException(nameof(checkFunc));
            _checkCollection.Add(checkFunc);
        }

        public async Task<ICollection<HealthcheckResult>> GetStatus(CancellationToken token)
        {
            var tasks = _checkCollection.Select(f => f.Check(token)).ToList();
            await Task.WhenAll(tasks).ConfigureAwait(false);
            return tasks.Select(x => x.Result).ToList();
        }
    }
}
