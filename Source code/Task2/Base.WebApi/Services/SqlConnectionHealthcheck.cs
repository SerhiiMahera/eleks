﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Base.WebApi.Interfaces;
using Base.WebApi.Models;
using Common.Library.Enums;

namespace Base.WebApi.Services
{
    public class SqlConnectionHealthcheck : IHealthCheck
    {
        private readonly string _connectionString;
        private readonly string _name;

        public SqlConnectionHealthcheck(string name, string connectionString)
        {
            _connectionString = connectionString;
            _name = name;
        }

        public async Task<HealthcheckResult> Check(CancellationToken cancellationToken = default(CancellationToken))
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                try
                {
                    await sqlConnection.OpenAsync(cancellationToken);
                    return new HealthcheckResult(HealthStatus.Healthy);
                }
                catch (SqlException)
                {
                    return new HealthcheckResult(HealthStatus.Unhealthy, $"Error when connect to {_name}");
                }
            }
        }
    }
}
