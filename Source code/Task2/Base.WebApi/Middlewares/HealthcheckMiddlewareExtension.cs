﻿using System;
using System.Linq;
using Base.WebApi.Builders;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Base.WebApi.Middlewares
{
    public static class HealthcheckMiddlewareExtension
    {
        public static IApplicationBuilder UseHealthchecks(this IApplicationBuilder app, string path)
        {
            return app.UseMiddleware<HealthcheckMiddleware>(path, TimeSpan.FromSeconds(20.0d));
        }

        public static IServiceCollection AddHealthchecks(this IServiceCollection services, Action<HealthcheckBuilder> buildAction)
        {
            var builder = new HealthcheckBuilder();
            buildAction(builder);
            return services.AddSingleton(builder);
        }
    }
}
