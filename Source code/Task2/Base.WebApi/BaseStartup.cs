﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Base.WebApi.Middlewares;
using Base.WebApi.Services;
using Common.Library.Interfaces;
using Common.Library.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace Base.WebApi
{
    public class BaseStartup
    {
        public BaseStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SwaggerOAuthOptions>(Configuration.GetSection("SwaggerClient"));
            services.Configure<OAuth2Options>(Configuration.GetSection("Auth"));
            services.AddCors(
            opt =>
             {
                 opt.AddPolicy("Default", builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
             });

            services.AddHealthchecks(x => { });

            services.AddAuthorization(opt =>
            {
                opt.AddPolicy("Admin", policy => policy.RequireRole("Admin"));
                opt.AddPolicy("User", policy => policy.RequireRole("User", "Admin"));
            });

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = Configuration["Auth:Authority"];
                    options.RequireHttpsMetadata = bool.Parse(Configuration["Auth:RequireHttps"]);
                    options.ApiName = Configuration["Auth:ApiName"];
                    options.SaveToken = true;
                });

            services.AddSwaggerGen(options =>
            {
                var authority = Configuration["Auth:Authority"];
                var apiName = Configuration["Auth:ApiName"];
                options.SwaggerDoc("v1", new Info { Title = "Test shop", Version = "v1" });
                options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });

                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                options.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "password",
                    AuthorizationUrl = authority + "/connect/authorize",
                    TokenUrl = authority + "/connect/token",
                    Scopes = new Dictionary<string, string> { { apiName, apiName + " API" } }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
            services.AddTransient<IBaseResponseError, BaseResponseError>();
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var authOpt = app.ApplicationServices.GetService<IOptions<OAuth2Options>>().Value;
            //app.UseHttpsRedirection();
            app.UseExceptionCatcher();
            app.UseCors("Default");
            app.UseHealthchecks("/health");
            app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                var swagger = app.ApplicationServices.GetService<IOptions<SwaggerOAuthOptions>>().Value;
                c.DocumentTitle = authOpt.ApiName;
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{authOpt.ApiName} API");
                c.DefaultModelExpandDepth(2);
                c.DefaultModelRendering(ModelRendering.Example);
                c.DefaultModelsExpandDepth(-1);
                c.DisplayOperationId();
                c.DisplayRequestDuration();
                c.DocExpansion(DocExpansion.None);
                c.EnableDeepLinking();
                c.EnableFilter();
                c.MaxDisplayedTags(10);
                c.ShowExtensions();
                // c.EnableValidator();
                c.SupportedSubmitMethods(SubmitMethod.Get,
                    SubmitMethod.Post,
                    SubmitMethod.Put,
                    SubmitMethod.Delete,
                    SubmitMethod.Head,
                    SubmitMethod.Options,
                    SubmitMethod.Patch,
                    SubmitMethod.Trace);
                //oauth2 configuration
                c.OAuthClientId(swagger.ClientId);
                c.OAuthClientSecret(swagger.ClientSecret);
                c.OAuthAppName(authOpt.ApiName);
                c.OAuth2RedirectUrl($@"{authOpt.Authority}/connect/token");
            });
            //if (!env.IsDevelopment())
            //{
            //    app.UseHsts();
            //}

            app.UseMvc();
        }
    }
}
