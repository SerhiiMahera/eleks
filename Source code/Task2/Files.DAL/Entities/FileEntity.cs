﻿using System;
using System.IO;
using Files.Common.Interfaces;

namespace Files.DAL.Entities
{
    public class FileEntity : IFileInfo
    {
        public Guid Id { get; set; }
        public Stream FileStream { get; set; }
        public DateTime Date { get; set; }
        public bool IsConfirmed { get; set; }
        public string ContentType { get; set; }
    }
}
