﻿using Base.DAL.Interfaces;

namespace Files.DAL.Interfaces
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        IFilesRepository FilesInfo { get; }
    }
}