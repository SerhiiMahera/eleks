﻿using System;
using System.Threading.Tasks;
using Files.Common.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Files.DAL.Interfaces
{
    public interface IFilesRepository
    {
        Task<IFileInfo> GetFileAsync(Guid id);
        Task<Guid> AddFileAsync(IFormFile file);
        Task RemoveUnusedFiles();
        void RemoveFile(Guid id);
    }
}